export default {
  apiKey: 'COPY_KEY_HERE', //register at developer.forecast.io
  url: 'https://api.forecast.io/forecast/', // API_KEY/LATITUDE,LONGITUDE ex. https://api.forecast.io/forecast/xxx/37.8267,-122.423
};

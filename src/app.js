'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

// needed for onTouchTap for Material-UI
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import Dashboard from './components/Dashboard.react.jsx';

ReactDOM.render(
  React.createElement(Dashboard),
  document.getElementById('dashboard')
);

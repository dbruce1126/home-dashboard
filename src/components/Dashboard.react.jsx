'use strict';

import React from 'react';
import Immutable from 'immutable';
import { GridList, GridTile } from 'material-ui/lib/grid-list';
import IconButton from 'material-ui/lib/icon-button';
import FontIcon from 'material-ui/lib/font-icon';
import Colors from 'material-ui/lib/styles/colors';

import Weather from './widgets/Weather.react.jsx';
import DateTime from './widgets/DateTime/DateTime.react.jsx';
import Settings from './widgets/Settings.react.jsx';
import Calendar from './widgets/Calendar.react.jsx';
import Metrics from './widgets/Metrics.react.jsx';
import Camera from './widgets/Camera.react.jsx';

import ViewStore from '../stores/ViewStore';
import DateTimeStore from '../stores/DateTimeStore';
import SettingsStore from '../stores/SettingsStore';
import WeatherStore from '../stores/WeatherStore';

const ROWS = 2;
const COLS = 3;
const PADDING = 2;

const DASHBOARD_STYLE = {
  fontFamily: 'Roboto,sans-serif',
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  overflow: 'hidden',
  userSelect: 'none',
  WebkitUserSelect: 'none',
};

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.storeStateChanged = this.storeStateChanged.bind(this);

    this.tileDimensions = new Immutable.Map({
      width: 0,
      height: 0
    });
  }
  componentDidMount() {
    // register to listening to stores
    ViewStore.addChangeListener(this.storeStateChanged);
    ViewStore.addWindowResizeListener();
    DateTimeStore.addChangeListener(this.storeStateChanged);
    DateTimeStore.startTick();
    SettingsStore.addChangeListener(this.storeStateChanged);
    WeatherStore.addChangeListener(this.storeStateChanged);
    WeatherStore.startTick();
  }
  componentWillUnmount() {
    // deregister from store listeners
    ViewStore.removeWindowResizeListener();
    ViewStore.removeChangeListener(this.storeStateChanged);
    DateTimeStore.stopTick();
    DateTimeStore.removeChangeListener(this.storeStateChanged);
    SettingsStore.removeChangeListener(this.storeStateChanged);
    WeatherStore.removeChangeListener(this.storeStateChanged);
  }
  storeStateChanged(storeWidget, key) {
    this.forceUpdate();
  }
  getLaunchIconForWidget(widget) {
    const maxWidgetCallback = ViewStore.maximizeWidget.bind(ViewStore, widget);
    return (
      <IconButton onClick={ maxWidgetCallback }>
        <FontIcon className="material-icons" color={ Colors.white }>
          { 'launch' }
        </FontIcon>
      </IconButton>
    );
  }
  updateTileDImensions(windowDimensions) {
    this.tileDimensions = this.tileDimensions.set(
      'width',
      (windowDimensions.get('width') / COLS) - (2 * PADDING)
    );
    this.tileDimensions = this.tileDimensions.set(
      'height',
      (windowDimensions.get('height') / ROWS) - (2 * PADDING)
    );
  }
  render() {
    const viewState = ViewStore.getCurrentState();
    this.updateTileDImensions(viewState.windowDimensions);
    const tileHeight = Math.ceil(viewState.windowDimensions.get('height') / ROWS);
    return (
      <GridList
        id="dashboard"
        cols={ 3 }
        rows={ 2 }
        cellHeight={ tileHeight }
        style={ DASHBOARD_STYLE }
        padding={ PADDING }>
        <GridTile key={ 'weather' } title={ 'Weather' }>
          <Weather dimensions={ this.tileDimensions } { ...WeatherStore.getCurrentState() }/>
        </GridTile>
        <GridTile
          key={ 'datetime' }
          title={ 'Date & Time' }
          actionIcon={ this.getLaunchIconForWidget('datetime') }>
          <DateTime
            { ...DateTimeStore.getCurrentState() }
            dimensions={ this.tileDimensions }
            maximized={ (viewState.maximizedWidget === 'datetime') } />
        </GridTile>
        <GridTile key={ 'calendar' } title={ 'Upcoming Events' }>
          <Calendar dimensions={ this.tileDimensions } />
        </GridTile>
        <GridTile key={ 'metrics' } title={ 'Hot Numbers' }>
          <Metrics dimensions={ this.tileDimensions } />
        </GridTile>
        <GridTile key={ 'camera' } title={ 'Camera' }>
          <Camera dimensions={ this.tileDimensions } />
        </GridTile>
        <GridTile key={ 'settings' } title={ 'Settings' }>
          <Settings { ...SettingsStore.getCurrentState() } dimensions={ this.tileDimensions } />
        </GridTile>
      </GridList>
    );
  }
}

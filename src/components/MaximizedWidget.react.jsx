'use strict';

import React from 'react';

import ViewStore from '../stores/ViewStore';

const SCREEN_GUTTER = 60;
const INNER_PADDING = 15;

export default class MaximizedWidget extends React.Component {
  static propTypes = {
    children: React.PropTypes.oneOfType([
      React.PropTypes.element,
      React.PropTypes.arrayOf(React.PropTypes.element),
    ]),
    style: React.PropTypes.objectOf(React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ])),
  };
  static defaultProps = {
    style: {},
  };
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(event) {
    // check if the click event fired within the modal maximized view
    if (event.target === this.refs.innerContent) {
      return;
    }
    // if not, minimize any maximized view
    ViewStore.minimizeWidget();
  }
  render() {
    let { style, ...otherProps } = this.props;
    const maxWidgetWrapperStyle = {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      overflow: 'hidden',
      zIndex: 1000,
      backgroundColor: 'rgba(0,0,0,0.6)',
    };
    style.position = 'absolute';
    style.left = SCREEN_GUTTER + 'px';
    style.top = SCREEN_GUTTER + 'px';
    style.right = SCREEN_GUTTER + 'px';
    style.bottom = SCREEN_GUTTER + 'px';
    style.overflow = 'auto';
    style.zIndex = 2000;
    if (!('padding' in style)) {
      style.padding = INNER_PADDING + 'px';
    }
    return (
      <div
        style={ maxWidgetWrapperStyle }
        onClick={ this.handleClick }
        ref="wrapper">
        <div
          style={ style }
          ref="innerContent">
          { this.props.children }
        </div>
      </div>
    );
  }
}

'use strict';

import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

export default class Widget extends React.Component {
  static propTypes = {
    children: React.PropTypes.oneOfType([
      React.PropTypes.element,
      React.PropTypes.arrayOf(React.PropTypes.element),
    ]),
    className: React.PropTypes.string,
    style: React.PropTypes.objectOf(React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ])),
    dimensions: ImmutablePropTypes.contains({
      height: React.PropTypes.number.isRequired,
      width: React.PropTypes.number.isRequired,
    }).isRequired,
  };
  static defaultProps = {
    className: '',
    style: {},
  };
  constructor(props) {
    super(props);
  }
  render() {
    let { className, style, ...otherProps } = this.props;
    style.width = '100%';
    style.height = '100%';
    if (!style.overflow && !style.overflowY) {
      style.overflowY = 'auto';
    }
    return (
      <div className={ ('widget ' + className).trim() } style={ style }
        { ...otherProps }>
        { this.props.children }
      </div>
    );
  }
}

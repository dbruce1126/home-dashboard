'use strict';

import React from 'react';
import Colors from 'material-ui/lib/styles/colors';
import FontIcon from 'material-ui/lib/font-icon';
import Table from 'material-ui/lib/table/table';
import TableBody from 'material-ui/lib/table/table-body';
import TableFooter from 'material-ui/lib/table/table-footer';
import TableHeader from 'material-ui/lib/table/table-header';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';

const DAY_OF_WEEK_MAP = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const MONTH_OF_YEAR_MAP = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const tableStyle = {
  width: '50%',
  backgroundColor: 'transparent',
};
const headerStyle = {
  textAlign: 'center',
  fontSize: '24px',
  color: Colors.white,
  paddingLeft: 0,
  paddingRight: 0,
};
const cellStyle = {
  fontSize: '20px',
  textAlign: 'center',
  color: Colors.white,
  paddingLeft: 0,
  paddingRight: 0,
};

// for the given month and year, compute an array of integers and nulls representing
// days of the month or empty slots in a calendar table
function computeWeeks(month, year) {
  // compute the number of days in this month
  const daysInMonth = new Date(year, month+1, 0).getDate();
  // compute the day of the week (the current month starts)
  let dayCounter = -(new Date(year, month, 1).getDay());
  let weeks = [];
  let currentWeek = [];
  while (dayCounter < daysInMonth) {
    if (++dayCounter <= 0) {
      currentWeek.push(null);
      continue;
    }
    currentWeek.push(dayCounter);
    if (currentWeek.length >= 7) {
      weeks.push(currentWeek);
      currentWeek = [];
    }
  }
  if (currentWeek.length > 0) {
    for (let i = currentWeek.length; i < 7; i++) {
      currentWeek.push(null);
    }
    weeks.push(currentWeek);
  }
  return weeks;
}

export default class Calendar extends React.Component {
  static propTypes = {
    month: React.PropTypes.number.isRequired,
    dayOfMonth: React.PropTypes.number.isRequired,
    year: React.PropTypes.number.isRequired
  };
  constructor(props) {
    super(props);
    this.weeks = computeWeeks(props.month, props.year);
  }
  static getNameOfMonth(month) {
    return (month in MONTH_OF_YEAR_MAP) ? MONTH_OF_YEAR_MAP[month] : 'Unknown';
  }
  static getNameOfWeekday(weekday) {
    return (weekday in DAY_OF_WEEK_MAP) ? DAY_OF_WEEK_MAP[weekday] : 'Unknown';
  }
  static getShortNameOfWeekday(weekday) {
    return Calendar.getNameOfWeekday(weekday).substring(0, 3);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.month !== this.props.month || nextProps.year !== this.props.year) {
      this.weeks = computeWeeks(nextProps.month, nextProps.year);
    }
  }
  render() {
    return (
      <Table height={ this.props.height + 'px' } style={ tableStyle } selectable={ false }>
        <TableHeader displaySelectAll={ false } adjustForCheckbox={ false }>
          <TableRow>
            <TableHeaderColumn colSpan="7" style={ headerStyle }>
              { Calendar.getNameOfMonth(this.props.month) } { this.props.year }
            </TableHeaderColumn>
          </TableRow>
          <TableRow>
            { [0,1,2,3,4,5,6].map((weekday) => (
              <TableHeaderColumn key={ 'weekday_' + weekday } style={ headerStyle }>
                { Calendar.getShortNameOfWeekday(weekday) }
              </TableHeaderColumn>
            )) }
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={ false }>
          { this.weeks.map((week, index) => {
            return (
              <TableRow key={ 'week_' + index }>
                { week.map((day, index)  => {
                  let icon = day;
                  if (day === this.props.dayOfMonth) {
                    icon = (
                      <FontIcon className="material-icons" color={ Colors.lime500 }>
                        { 'star' }
                      </FontIcon>
                    );
                  }
                  return (
                    <TableRowColumn key={ 'day_' + index } style={ cellStyle }>
                      { icon }
                    </TableRowColumn>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  }
}
'use strict';

import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

function getSuffix(number) {
  if (number >= 4 && number <= 20) {
    // handle the "teens"
    return 'th';
  } else {
    switch (number % 10) {
      case 1:
        return 'st';
      case 2:
        return 'nd';
      case 3:
        return 'rd';
      default:
        return 'th';
    }
  }
}

const style = {
  fontSize: '40px',
  fontWeight: 'lighter',
  margin: '0',
};

export default class DateAndMonthDisplay extends React.Component {
  static propTypes = {
    dayOfMonth: React.PropTypes.number.isRequired,
    month: React.PropTypes.string.isRequired,
  };
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  render() {
    return (
      <h2 style={ style }>
        { this.props.month } { this.props.dayOfMonth }{ getSuffix(this.props.dayOfMonth) }
      </h2>
    );
  }
}
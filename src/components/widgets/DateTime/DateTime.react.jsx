'use strict';

import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Colors from 'material-ui/lib/styles/colors';

import Widget from '../../Widget.react.jsx';
import MaximizedWidget from '../../MaximizedWidget.react.jsx';

// sub components
import TimeDisplay from './TimeDisplay.react.jsx';
import DayOfWeekDisplay from './DayOfWeekDisplay.react.jsx';
import DateAndMonthDisplay from './DateAndMonthDisplay.react.jsx';
import YearDisplay from './YearDisplay.react.jsx';
import Calendar from './Calendar.react.jsx';

const BACKGROUND_COLOR = Colors.teal500;

const style = {
  backgroundColor: BACKGROUND_COLOR,
  color: Colors.white,
  textAlign: 'center',
  paddingTop: '40px',
};

export default class DateTime extends React.Component {
  static propTypes = {
    second: React.PropTypes.number.isRequired,
    minute: React.PropTypes.number.isRequired,
    hour: React.PropTypes.number.isRequired,
    dayOfWeek: React.PropTypes.number.isRequired,
    month: React.PropTypes.number.isRequired,
    dayOfMonth: React.PropTypes.number.isRequired,
    year: React.PropTypes.number.isRequired,
    dimensions: ImmutablePropTypes.contains({
      height: React.PropTypes.number.isRequired,
      width: React.PropTypes.number.isRequired,
    }).isRequired,
    maximized: React.PropTypes.bool
  };
  static defaultProps = {
    maximized: false
  };
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  getMaximizedWidget() {
    if (!this.props.maximized) {
      return null;
    }
    return (
      <MaximizedWidget style={{ backgroundColor: BACKGROUND_COLOR }}>
        <Calendar
          dayOfMonth={ this.props.dayOfMonth }
          month={ this.props.month }
          year={ this.props.year } />
      </MaximizedWidget>
    );
  }
  render() {
    return (
      <Widget className="datetime" style={ style } dimensions={ this.props.dimensions }>
        <TimeDisplay
          second={ this.props.second }
          minute={ this.props.minute }
          hour={ this.props.hour } />
        <DayOfWeekDisplay dayOfWeek={ Calendar.getNameOfWeekday(this.props.dayOfWeek) } />
        <DateAndMonthDisplay
          dayOfMonth={ this.props.dayOfMonth }
          month={ Calendar.getNameOfMonth(this.props.month) } />
        <YearDisplay year={ this.props.year } />
        { this.getMaximizedWidget() }
      </Widget>
    );
  }
}
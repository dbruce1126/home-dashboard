'use strict';

import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

const style = {
  fontSize: '40px',
  fontWeight: 'bold',
  margin: '0',
};

export default class DayOfWeekDisplay extends React.Component {
  static propTypes = {
    dayOfWeek: React.PropTypes.string.isRequired,
  };
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  render() {  
    return (
      <h2 style={ style }>
        { this.props.dayOfWeek }
      </h2>
    );
  }
}
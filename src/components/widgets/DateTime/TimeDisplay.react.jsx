'use strict';

import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

function formatHour(hour) {
  return (hour > 12) ? hour - 12 : ((hour === 0) ? 12 : hour);
}

function padWithZeroes(count) {
  return (count > 9) ? count : '0' + count;
}

function getAmPm(hour) {
  return (hour === 0) ? 'AM' : ( (hour >= 12) ? 'PM' : 'AM');
}

const style = {
  fontSize: '50px',
  fontWeight: 'lighter',
  margin: '0',
};

export default class TimeDisplay extends React.Component {
  static propTypes = {
    second: React.PropTypes.number.isRequired,
    minute: React.PropTypes.number.isRequired,
    hour: React.PropTypes.number.isRequired,
  };
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  render() {
    return (
      <h1 style={ style }>
        { formatHour(this.props.hour) }:{ padWithZeroes(this.props.minute) }:{ padWithZeroes(this.props.second) } { getAmPm(this.props.hour) }
      </h1>
    );
  }
}
'use strict';

import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

const style = {
  fontSize: '30px',
  fontWeight: 'lighter',
  margin: '0',
};

export default class YearDisplay extends React.Component {
  static propTypes = {
    year: React.PropTypes.number.isRequired,
  };
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  render() {
    return (
      <h2 style={ style }>
        { this.props.year }
      </h2>
    );
  }
}
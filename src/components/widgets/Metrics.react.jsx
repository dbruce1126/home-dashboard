'use strict';

import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Colors from 'material-ui/lib/styles/colors';

import Widget from '../Widget.react.jsx';

export default class Metrics extends React.Component {
  static propTypes = {
    dimensions: ImmutablePropTypes.contains({
      height: React.PropTypes.number.isRequired,
      width: React.PropTypes.number.isRequired,
    }).isRequired,
  };
  constructor(props) {
    super(props);
  }
  render() {
    const style = {
      backgroundColor: Colors.orange500,
      color: Colors.black,
    };
    return (
      <Widget className="metrics" style={ style } dimensions={ this.props.dimensions } />
    );
  }
}

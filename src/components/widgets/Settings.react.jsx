'use strict';

import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Colors from 'material-ui/lib/styles/colors';
import Paper from 'material-ui/lib/paper';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import Toggle from 'material-ui/lib/toggle';

import Widget from '../Widget.react.jsx';

import SettingsStore from '../../stores/SettingsStore';

export default class Settings extends React.Component {
  static propTypes = {
    fullScreen: React.PropTypes.bool.isRequired,
    dimensions: ImmutablePropTypes.contains({
      height: React.PropTypes.number.isRequired,
      width: React.PropTypes.number.isRequired,
    }).isRequired,
  };
  constructor(props) {
    super(props);
  }
  getFullScreenToggle() {
    return (
      <Toggle
        defaultToggled={ this.props.fullScreen }
        onToggle={ SettingsStore.toggleFullScreen.bind(SettingsStore) } />
    );
  }
  render() {
    const style = {
      backgroundColor: Colors.white,
      color: Colors.text,
    };
    return (
      <Widget className="settings" style={ style } dimensions={ this.props.dimensions }>
        <List subheader="Dashboard Settings" style={ { borderRadius: 0, paddingBottom: '65px' } }>
          <ListItem primaryText="Fullscreen" rightToggle={ this.getFullScreenToggle() } />
        </List>
      </Widget>
    );
  }
}

'use strict';

import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Colors from 'material-ui/lib/styles/colors';
import Skycons from 'react-skycons';
import Widget from '../Widget.react.jsx';

export default class Weather extends React.Component {
  static propTypes = {
    dimensions: ImmutablePropTypes.contains({
      height: React.PropTypes.number.isRequired,
      width: React.PropTypes.number.isRequired,
    }).isRequired,
    weather: React.PropTypes.shape({
      currently: React.PropTypes.shape({
        summary: React.PropTypes.string.isRequired,
        icon: React.PropTypes.string.isRequired,
        temp: React.PropTypes.number.isRequired,
        humidity: React.PropTypes.number.isRequired,
        precipProbability: React.PropTypes.number.isRequired,
        weekSummary: React.PropTypes.string.isRequired,
        weekIcon: React.PropTypes.string.isRequired,
      }),
      daily: React.PropTypes.arrayOf(React.PropTypes.object),
    }).isRequired,
    isLoaded: React.PropTypes.bool.isRequired,
  };
  constructor(props) {
    super(props);
  }
  getForecast() {
    const style = {
      fontSize: '40px',
      fontWeight: 'lighter',
      margin: '0',
    };
    //const iconClass = 'wi-forecast-io-' + this.props.weather.currently.icon;
    return (
      <h2 style={ style }>
        <Skycons color="white" icon={ this.props.weather.currently.icon }
          autoplay={ true } width="100" height="100"/>
      { this.props.weather.currently.temp }
      </h2>
    );
  }
  getWidgetContents() {
    if (!this.props.isLoaded) {
      return (
          <h4> { 'Loading...' } </h4>
      );
    } else {
      return (
        this.getForecast()
      );
    }
  }
  render() {
    const style = {
      backgroundColor: Colors.purple500,
      color: Colors.white,
      textAlign: 'center',
      paddingTop: '40px',
    };
    return (
      <Widget className="weather" style={ style }
        dimensions={ this.props.dimensions }>
        { this.getWidgetContents() }
      </Widget>
    );
  }
}

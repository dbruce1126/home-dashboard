'use strict';

import settings from '../../settings.js';
import jsonp from 'browser-jsonp';

export default class Forecast {
  constructor() {
    if (!settings) {
      console.error('No apikey or url for forecast API');
    }
    this.apiKey = settings.apiKey;
    const fURL = settings.url;
    this.url = fURL + this.apiKey + '/';
  }
  getForecast(opts, callback) {
    if (!opts) throw new Error('You must pass in an arguments hash with lat/long (optional time)');
    if (!opts.lat) throw new Error('You must pass in a valid latitude to get a forecast');
    if (!opts.lon) throw new Error('You must pass in a valid longitude to get a forecast');

    let constructURL = this.url + opts.lat + ',' + opts.lon;
    jsonp({
      url: constructURL,
      data: { units: 'ca' },
      success: function(data) { window.weatherCallback(data); },
    });
  }
}

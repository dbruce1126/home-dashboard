'use strict';

import EventEmitter from 'events';

const CHANGE_EVENT = 'change';

export default class AbstractStore extends EventEmitter {
  constructor(initialState) {
    super();
    this.currentState = {};
    if (initialState) {
      this.currentState = initialState;
    }
  }
  getCurrentState() {
    return this.currentState;
  }
  addChangeListener(listener) {
    this.on(CHANGE_EVENT, listener);
    return this;
  }
  removeChangeListener(listener) {
    this.removeListener(CHANGE_EVENT, listener);
    return this;
  }
  emitChange() {
    this.emit(CHANGE_EVENT);
    return this;
  }
}

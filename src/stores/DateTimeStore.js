'use strict';

import AbstractStore from './AbstractStore';

class DateTimeStore extends AbstractStore {
  constructor(window) {
    super(new Date());
    this.window = window;
  }
  getCurrentState() {
    return {
      second: this.currentState.getSeconds(),
      minute: this.currentState.getMinutes(),
      hour: this.currentState.getHours(),
      dayOfWeek: this.currentState.getDay(),
      month: this.currentState.getMonth(),
      dayOfMonth: this.currentState.getDate(),
      year: this.currentState.getFullYear(),
    };
  }
  startTick() {
    if (this.timer) {
      return;
    }
    this.timer = this.window.setInterval(function() {
      this.currentState = new Date();
      this.emitChange();
    }.bind(this), 100);
  }
  stopTick() {
    if (!this.timer) {
      return;
    }
    this.window.clearInterval(this.timer);
  }
}

const fallback = {
  setInterval: function() {
    console.warn('Something went wrong, using fallback window object.');
  },
  clearInterval: function() {
    console.warn('Something went wrong, using fallback window object.');
  },
};
let storeInstance = (typeof window === 'object') ? new DateTimeStore(window) : new DateTimeStore(fallback);
export default storeInstance;

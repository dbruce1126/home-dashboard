'use strict';

import AbstractStore from './AbstractStore';

class SettingsStore extends AbstractStore {
  constructor(window) {
    super({
      fullScreen: false,
    });
    this.window = window;
  }
  toggleFullScreen() {
    if (this.currentState.fullScreen) {
      this.exitFullScreen();
    } else {
      this.enterFullScreen();
    }
  }
  enterFullScreen() {
    var dashboard = this.window.document.getElementById('dashboard');
    if (dashboard.requestFullscreen) {
      dashboard.requestFullScreen();
    } else if (dashboard.webkitRequestFullscreen) {
      dashboard.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    } else if (dashboard.mozRequestFullScreen) {
      dashboard.mozRequestFullScreen();
    } else if (dashboard.msRequestFullscreen) {
      dashboard.msRequestFullscreen();
    } else {
      console.warn('Fullscreen API not enabled.');
      this.currentState.fullScreen = false;
      return;
    }
    this.currentState.fullScreen = true;
  }
  exitFullScreen() {
    if (this.window.document.exitFullscreen) {
      this.window.document.exitFullscreen();
    } else if (this.window.document.webkitExitFullscreen) {
      this.window.document.webkitExitFullscreen();
    } else if (this.window.document.mozCancelFullscreen) {
      this.window.document.mozCancelFullscreen();
    } else if (this.window.document.msExitFullscreen) {
      this.window.document.msExitFullscreen();
    } else {
      console.warn('Fullscreen API not enabled.');
    }
    this.currentState.fullScreen = false;
  }
}

const fallback = {
  document: {
    getElementById: function() {
      console.warn('Something went wrong, using fallback window object.');
      return {};
    },
  },
};
let storeInstance = (typeof window === 'object') ? new SettingsStore(window) : new SettingsStore(fallback);
export default storeInstance;

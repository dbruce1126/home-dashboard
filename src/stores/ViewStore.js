'use strict';

import Immutable from 'immutable';
import AbstractStore from './AbstractStore';

class ViewStore extends AbstractStore {
  constructor(window) {
    super({
      fullScreen: false,
      maximizedWidget: null,
      windowDimensions: Immutable.Map({
        width: window.innerWidth,
        height: window.innerHeight,
      }),
    });
    this.window = window;

    this.maximizeWidget = this.maximizeWidget.bind(this);
    this.minimizeWidget = this.minimizeWidget.bind(this);
  }
  maximizeWidget(widget) {
    this.currentState.maximizedWidget = widget;
    this.emitChange();
  }
  minimizeWidget() {
    this.currentState.maximizedWidget = null;
    this.emitChange();
  }
  addWindowResizeListener() {
    this.window.addEventListener('resize', this.windowResized.bind(this));
  }
  removeWindowResizeListener() {
    this.window.removeEventListener('resize', this.windowResized.bind(this));
  }
  windowResized() {
    this.currentState.windowDimensions = this.currentState.windowDimensions.set(
      'width',
      this.window.innerWidth
    );
    this.currentState.windowDimensions = this.currentState.windowDimensions.set(
      'height',
      this.window.innerHeight
    );
    this.emitChange();
  }
}

const fallback = {
  innerWidth: 0,
  innerHeight: 0,
  document: {
    getElementById: function() {
      console.warn('Something went wrong, using fallback window object.');
      return {};
    },
  },
  addEventListener: function() {
    console.warn('Something went wrong, using fallback window object.');
  },
  removeEventListener: function() {
    console.warn('Something went wrong, using fallback window object.');
  },
};
let storeInstance = (typeof window === 'object') ? new ViewStore(window) : new ViewStore(fallback);
export default storeInstance;

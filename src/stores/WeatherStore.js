'use strict';

import AbstractStore from './AbstractStore';
import Forecast from '../lib/Forecast-io.js';

const DEFAULT_WEATHER = {
  weather: {
    currently: {
      summary: '',
      icon: '',
      temp: 0,
      humidity: 0,
      precipProbability: 0,
      weekSummary: '',
      weekIcon: '',
    },
    daily: [],
  },
  isLoaded: false,
};

class WeatherStore extends AbstractStore {
  constructor(window) {
    super(DEFAULT_WEATHER);
    this.window = window;
    this.window.weatherCallback = this.weatherCallback.bind(this);
    this.weather = new Forecast();
    this.weather.getForecast(this.getLocation());
  }
  getCurrentState() {
    return this.currentState;
  }
  startTick() {
    if (this.timer) {
      return;
    }
    this.timer = setTimeout(function() {
      this.weather.getForecast(this.getLocation());
      this.emitChange();
    }.bind(this), 2000);
  }
  stopTick() {
    if (!this.timer) {
      return;
    }
    clearInterval(this.timer);
  }
  getLocation() {
    return {
      lat: 45.4214,
      lon: -75.6919,
    };
  }
  transformIconText(text) {
    if (!text) {
      return '';
    }
    return text.toString().toUpperCase().replace(/-/g, '_');
  }
  weatherCallback(data) {
    data.daily.data = data.daily.data.map( (day) => {
      return {
        summary: day.summary,
        temperatureMin: day.temperatureMin,
        temperatureMax: day.temperatureMax,
        humidity: day.humidity,
        icon: this.transformIconText(day.icon),
        precipProbability: day.precipProbability,
      };
    });
    this.currentState = {
      weather: {
        currently: {
          summary: data.currently.summary,
          icon: this.transformIconText(data.currently.icon),
          temp: data.currently.temperature,
          humidity: data.currently.humidity,
          precipProbability: data.currently.precipProbability,
          weekSummary: data.daily.summary,
          weekIcon: this.transformIconText(data.daily.icon),
        },
        daily: data.daily.data,
      },
      isLoaded: true,
    };
  }
}
let weatherInstance = new WeatherStore(window);
export default weatherInstance;
